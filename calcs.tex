\documentclass[a4paper]{article}
\usepackage[margin=1.75cm]{geometry}
\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\usepackage{multicol}
\setlength{\columnsep}{.75cm}

\usepackage{hyperref, float, booktabs}
\usepackage{amsmath, commath, bm}
\usepackage{siunitx, mhchem}
\usepackage{tikz}

\usepackage{caption}
\usepackage[newfloat]{minted}
\usemintedstyle{friendly}
\newminted{pycon}{bgcolor=white, linenos=false, tabsize=4}
\newenvironment{code}{\captionsetup{type=listing}}{}
\SetupFloatingEnvironment{listing}{name=Code}

\usepackage{pdflscape}
\usepackage{afterpage}
\usepackage{capt-of}

\newcount\colveccount
\newcommand*\colvec[1]{
  \global\colveccount#1
  \begin{pmatrix}
  \colvecnext
  }
  \def\colvecnext#1{
  #1
  \global\advance\colveccount-1
  \ifnum\colveccount>0
  \\
  \expandafter\colvecnext
  \else
  \end{pmatrix}
  \fi
}

\title{Calculating TEXT}
\author{Peleg Bar Sapir}
\date{\today}

\begin{document}
\maketitle

\begin{multicols}{2}
\section{Introduction}
\subsection{Goal}
\subsection{A Note About Units}
Since most nutrient sufficency tables use \si{ppm} for concentration, we will use it as well. It is important to note that this refers to \textit{mass} fraction, i.e. 1 \si{ppm} = 1 \si{mg} per \si{kg} water (since 1 \si{Liter} of water has a mass of 1 \si{kg}).

\section{Calculation}
\subsection{Deriving a Calculation Method via a Simple Example}
For the first example we will use the following NPK target concentrations (in \si{ppm}): 150, 15, 50. We assume that we have the following three chemical salts available: Ammonium nitrate (\ce{NH4NO3}), Dipotassium phosphate (\ce{K2HPO4}) and Potassium nitrate (\ce{KNO3}).

We start by calculating the resulting total nitrogen concentration after adding 1 gram of \ce{NH4NO3} to 1 \si{Liter} of water. The mass fraction $m_{f}$ of nitrogen in \ce{NH4NO3} can be calculated using the molar masses of the nitrogen atom and \ce{NH4NO3}:
\begin{align}
	m_{f} = \frac{2\cdot M_{\ce{N}}}{M_{\ce{NH4NO3}}} = \frac{2\cdot14.01}{80.04} = 0.35.
	\label{eq:mass_fraction_nitrogen}
\end{align}
Thus, each gram of \ce{NH4NO3} added to the solution adds to it 0.35 grams (350 \si{ppm}) of nitrogen, and adding 430 \si{mg} of \ce{NH4NO3} will yield the desired target concentration (150 \si{ppm}) of nitrogen, within an error of $\pm1\ \si{ppm}$ (since $0.43\times350=150.5$).

Next, we run the same calculation for phosphorus and potassium using \ce{K2HPO4}, and we get
\begin{align}
	m_{f}^{\ce{P}}&=0.178,\\
	m_{f}^{\ce{K}}&=0.244.
	\label{eq:mass_fraction_phosphorus_K2HPO4}
\end{align}

Adding 84.3 \si{mg} of \ce{K2HPO4} to the solution will yield the phosphorus concentration target of 15 \ce{ppm}. However, if we wish to get 50 \si{ppm} of potassium in the solution, we would have to add 204.9 \si{mg} of \ce{K2HPO4} to the solution, which would cause the phosphorus concentration to be 36.5 \si{ppm} - more than twice the target concentration. We could just add 84.3 \si{mg} of \ce{K2HPO4}, yielding 20.6 \si{ppm} of potassium, and use \ce{KNO3} (the last available salt) for the additional supply of 29.4 \si{ppm} of potassium. However, that will cause us to overshoot the nitrogen concentration, since dissolved \ce{KNO3} also adds to the concentration of nitrogen in the solution.

The above problem points to the fact that any robust method which calculates the amounts needed of each salt must perform the calculation in conjunction to all other salts, rather than concurrently.

Let's re-write the problem: essentially, we are looking for three quantities $\alpha,\beta,\gamma$ such that $\alpha$ grams of \ce{NH4NO3} + $\beta$ grams of \ce{K2HPO4} + $\gamma$ grams of \ce{KNO3} will yield 150 \si{ppm} nitrogen, 15 \si{ppm} phosphorus and 50 \si{ppm} potassium. Each of the salts adds to the solution a certain concentration of each of our elements: as stated above, each gram of \ce{K2HPO4} adds 0 \si{ppm} of nitrogen, 178 \si{ppm} of phosphorus and 244 \si{ppm} of potassium, which we can write as a column vector, which we'll call the \textit{mass fraction vector}:
\begin{equation}
	\vec{F}_{1} = \vec{F}_{\ce{K2HPO4}} = \colvec{3}{0}{178}{244}.
	\label{eq:C_K2HPO4}
\end{equation}

We can write the other two salts as vectors too:
\begin{align}
	\vec{F}_{2} &= \vec{F}_{\ce{NH4NO3}} = \colvec{3}{350}{0}{0},\\
	\vec{F}_{3} &= \vec{F}_{\ce{KNO3}} = \colvec{3}{139}{0}{387}.
	\label{eq:C_rest}
\end{align}
(note: the units of these vectors are [\si{ppm/gr}])

Our problem is then reduced to solving the following system of equations:
\begin{equation}
	\alpha\vec{F}_{1} + \beta\vec{F}_{2} + \gamma\vec{F}_{3} = \colvec{3}{150}{15}{50},
	\label{eq:problem_equation}
\end{equation}
which can be written explicitely as
\begin{equation}
	\begin{pmatrix}
		0 & 350 & 139\\
		178 & 0 & 0\\
		244 & 0 & 387
	\end{pmatrix}\colvec{3}{\alpha}{\beta}{\gamma} = \colvec{3}{150}{15}{50}.
	\label{eq:problem_equation_matrix}
\end{equation}

Equation \ref{eq:problem_equation_matrix} is very easy for computers to solve, and the solution is
\begin{equation}
	\colvec{3}{\alpha}{\beta}{\gamma} = \colvec{3}{0.0843}{0.3983}{0.0761}.
	\label{eq:solution1}
\end{equation}

Code \ref{code:python1} shows an example of using python with the numpy library to solve Equation \ref{eq:problem_equation_matrix}.

\begin{code}
	\begin{pyconcode}
>>> import numpy as np
>>> A = np.array([[0, 350, 139],
...               [178, 0,   0],
...               [244, 0, 387]])
>>> v = np.array([150,15,50])
>>> x = np.linalg.solve(A,v)
>>> x
array([0.08426966, 0.39836168, 0.07606771])
		\end{pyconcode}
		\captionof{listing}{Using python to solve Equation \ref{eq:problem_equation_matrix}.}
	\label{code:python1}
	\end{code}

Since each matrix entry has units of [\si{ppm/gr}], and the right most vector has units of [\si{ppm}], the solution vector\footnote{\textit{solution} in the mathemaical sense of the word.} is given in units of grams. That is great, since it gives us the exact quantity of each salt that we need to add to the solution in order to achieve the target concentrations of NPK. Let's validate it:

\begin{enumerate}
	\item 0.0843 grams of \ce{K2HPO4} give us $0.0843\times178=15$ \si{ppm} of phosphorus, and $0.0843\times244=20.57$ \si{ppm} of potassium.
	\item 0.3983 grams of \ce{NH4NO3} give us $0.3983\times350=139.4$ \si{ppm} of nitrogen.
	\item 0.0761 grams of \ce{KNO3} give us $0.0761\times139=10.6$ \si{ppm} of nitrogen. Together with the 139.4 \si{ppm} from the \ce{NH4NO3} we get exactly 150 \si{ppm} of nitrogen. We also get $0.0761\times387=29.45$ \si{ppm} of potassium, which together with the potassium from the \ce{K2HPO4} yeilds a total of 50 \si{ppm} of potassium.
\end{enumerate}

As we can see, the method yields the expected result.

\subsection{A More Complicated Example}\label{sec:big_example}
We wish to prepare a complete nutrient solution\footnote{excluding trace elements such as iron, copper, zinc, etc.}, as described in Table \ref{tab:full_nutrient_solution}, using the following salts: \ce{KNO3}, \ce{K2HPO4}, \ce{Ca(NO3)2}, \ce{MgSO4.7H2O}, \ce{CaCl2}, \ce{K2SO4} and \ce{Ca(OH)2}.
\begin{table}[H]
	\centering
	\caption{Text}
	\label{tab:full_nutrient_solution}
	\begin{tabular}{ll}
		\toprule
		Element & ppm\\
		\midrule
		Nitrogen & 210\\
		Phosphorus & 31\\
		Potassium & 235\\
		Calcium & 270\\
		Magnesium & 49\\
		Sulfur & 90\\
		Chloride & 50\\
		\bottomrule
	\end{tabular}
\end{table}

For each available salt, we write a mass fraction vector:
\begin{align}
	\vec{F}_{1} &= \colvec{7}{139}{0}{387}{0}{0}{0}{0},\ \vec{F}_{2} = \colvec{7}{0}{178}{244}{0}{0}{0}{0},\ \vec{F}_{3}=\colvec{7}{171}{0}{0}{244}{0}{0}{0}\\\nonumber
	\vec{F}_{4} &= \colvec{7}{0}{0}{0}{0}{99}{130}{0},\ \vec{F}_{5} = \colvec{7}{0}{0}{0}{361}{0}{0}{639},\ \vec{F}_{6} = \colvec{7}{0}{0}{224}{0}{0}{184}{0},\\\nonumber
	\vec{F}_{7} &= \colvec{7}{0}{0}{0}{541}{0}{0}{0},
	\label{eq:mass_fraction_vectors}
\end{align}
and solve the equation
\begin{equation}
	\setlength\arraycolsep{1pt}
	\begin{pmatrix}
		139 &	0 &	119 &	0 &	0 &	0 &	0\\
		0 &	178 &	0 &	0 &	0 &	0 &	0\\
		387 &	449 &	0 &	0 &	0 &	224 &	0\\
		0 &	0 &	170 &	0 &	361 &	0 &	541\\
		0 &	0 &	0 &	99 &	0 &	0 &	0\\
		0 &	0 &	0 &	130 &	0 &	184 &	0\\
		0 &	0 &	0 &	0 &	639 &	0 &	0\\
	\end{pmatrix}\colvec{7}{x_{1}}{x_{2}}{x_{3}}{x_{4}}{x_{5}}{x_{6}}{x_{6}} = \colvec{7}{210}{31}{235}{270}{49}{90}{50},
	\label{eq:big_matrix}
\end{equation}

yielding the solution
\begin{equation}
	\vec{x} = \colvec{7}{x_{1}}{x_{2}}{x_{3}}{x_{4}}{x_{5}}{x_{6}}{x_{6}} = \colvec{7}{0.324}{0.174}{1.386}{0.495}{0.078}{0.139}{0.011}.
	\label{eq:big_solution}
\end{equation}

\subsection{Limitations of the Method}
A major limitation of the presented method is that for target concentrations that are too small, the solution for one or more of the salts used might be negative. For example, we can re-run the calculations in section \ref{sec:big_example} with a lower target concentration for sulfur, e.g. 45 \si{ppm} instead of 90 \si{ppm}. This yields the following solution vector:
\begin{equation}
	\vec{x} = \colvec{7}{0.466}{0.174}{1.220}{0.495}{0.078}{-0.105}{0.063},
	\label{eq:negative_amounts}
\end{equation}
which means that an amount of -0.105 grams of \ce{K2SO4} is required to reach the target concentrations. This is, obviously, and obsurd result.
\end{multicols}

\section{Appendices?}
\afterpage{
	\clearpage
	\thispagestyle{empty}
	\begin{landscape}
		\begin{table}
			\caption{Mass fraction vectors (in \si{ppm/gram}) for macronutrients in common salts}
			\label{tab:mass_fraction_vectors}
			\centering
			\begin{tabular}{lcccccccccccc}
				\toprule
			Element & \ce{NH4NO3} & \ce{KNO3} & \ce{K2HPO4} & \ce{KH2PO4} & \ce{K2SO4} & \ce{Ca(NO3)2} & \ce{Ca(NO3)2.4H2O} & \ce{MgSO4.7H2O} & \ce{CaCl} & \ce{Ca(OH)2} & \ce{K2CO3} & \ce{KOH} \\
			\midrule
			\ce{N} & 349 & 138 & 0 & 0 & 0 & 170 & 118 & 0 & 0 & 0 & 0 & 0 \\
			\ce{P} & 0 & 0 & 177 & 227 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
			\ce{K} & 0 & 386 & 448 & 287 & 448 & 0 & 0 & 0 & 0 & 0 & 565 & 696 \\
			\ce{Ca} & 0 & 0 & 0 & 0 & 0 & 244 & 169 & 0 & 530 & 540 & 0 & 0 \\
			\ce{Mg} & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 98 & 0 & 0 & 0 & 0 \\
			\ce{S} & 0 & 0 & 0 & 0 & 183 & 0 & 0 & 130 & 0 & 0 & 0 & 0 \\
			\ce{Cl} & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 469 & 0 & 0 & 0 \\
			\bottomrule
		\end{tabular}
	\end{table}
\end{landscape}
}
\end{document}
