#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-

from chemparse import parse_formula as parse
import json
import numpy as np
from numpy.linalg import LinAlgError as LAErr


NPK = ['N', 'P', 'K']
N = 'N'
P = 'P'
K = 'K'
Ca = 'Ca'
Mg = 'Mg'
S = 'S'
Cl = 'Cl'
Na = 'Na'
Fe = 'Fe'
Mn = 'Mn'
Zn = 'Zn'
B = 'B'
Cu = 'Cu'
Mo = 'Mo'
Si = 'Si'
O = 'O'
Br = 'Br'


with open('atomic_mass.json', 'r') as f:
    atomic_mass = json.load(f)['mass']

def MW(material):
    elements = parse(material)
    return sum([n*atomic_mass[A] for A, n in elements.items()])

def create_mfvec(material, basis=NPK):
    mw = MW(material)
    vec = np.zeros(len(basis))
    elements = parse(material)
    for i, el in enumerate(basis):
        if el in elements:
            vec[i] = elements[el]*atomic_mass[el]/mw
    vec = [elements[el]*atomic_mass[el]/mw
           if el in elements else 0
           for el in basis]
    return np.array(vec)


def calc_salt_amounts(target, salts, basis=NPK, scale=1000):
    M = np.zeros((len(target), len(salts)))
    for i, salt in enumerate(salts):
        M[:,i] = create_mfvec(salt, basis) * scale
    try:
        x = np.linalg.solve(M, target)
    except:
        #raise LAErr('Singular matrix')
        #pass
        return np.zeros(len(salts)), M
    return x, M
