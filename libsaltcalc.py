#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-

from chemparse import parse_formula as parse
import json
import numpy as np


NPK = ['N', 'P', 'K']

with open('atomic_mass.json', 'r') as f:
    atomic_mass = json.load(f)['mass']

def create_mfvec(material, basis=NPK):
    elements = parse(material)
    mw = sum([n*atomic_mass[A] for A, n in elements.items()])
    vec = np.zeros(len(basis))
    for i, el in enumerate(basis):
        if el in elements:
            vec[i] = elements[el]*atomic_mass[el]/mw
    vec = [elements[el]*atomic_mass[el]/mw
           if el in elements else 0
           for el in basis]
    return np.array(vec)

def calc_salt_amounts(target, salts, basis=NPK, scale=1000):
    M = np.zeros((len(target), len(salts)))
    for i, salt in enumerate(salts):
        M[:,i] = create_mfvec(salt, basis) * scale
    return np.linalg.solve(M, target), M
